FROM nginx:stable-alpine

ADD ./nginx/nginx.conf /etc/nginx/nginx.conf
ADD ./nginx/default.conf /etc/nginx/conf.d/default.conf

RUN mkdir -p /var/www/html

RUN addgroup -g 1024 landing && adduser -G landing -g landing -s /bin/sh -D landing

RUN chown -R landing:landing /var/www/html