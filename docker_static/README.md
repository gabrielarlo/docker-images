## Usage

After cloning run this inside the root directory
```
docker-compose up -d --build site
```
Bringing up the Docker Compose network with `site` instead of just using `up`, ensures that only site's container are brought up at the start, instead of all the command container as well. The following are built for our web server, with their exposed ports detailed:

- **nginx** - `:80`
- **nginx** - `:443`

## PREPARING APP

The source is empty at this moment. To create, copy your existing html directory inside the `src`.
just make sure the root of your html project is the `src` folder.
NOTE: `src` folder in ignored so create separate version control inside `src` folder