## Build and RUN

`docker-compose up -d`

## Working Directory

`wordpress`

## RDS Database
Just Change the values:
```
environment:
    - WORDPRESS_DB_NAME=wpdb
    - WORDPRESS_TABLE_PREFIX=wp_
    - WORDPRESS_DB_HOST=mysql
    - WORDPRESS_DB_PASSWORD=aqwe123
```

### Troubleshooting
- Rename the `container_name` by adding suffix or prefix