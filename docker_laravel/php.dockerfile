FROM php:7.3-fpm-alpine

ADD ./php/www.conf /usr/local/etc/php-fpm.d/www/conf

RUN addgroup -g 1024 laravel && adduser -G laravel -g laravel -s /bin/sh -D laravel

RUN mkdir -p /var/www/html

RUN chown -R laravel:laravel /var/www/html

RUN chmod -R 775 /var/www/html

RUN chown -R www-data:www-data /var/www/html

WORKDIR /var/www/html

RUN docker-php-ext-install pdo pdo_mysql