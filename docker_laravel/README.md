## Usage

After cloning run this inside the root directory
```
docker-compose up -d --build site
```
Bringing up the Docker Compose network with `site` instead of just using `up`, ensures that only site's container are brought up at the start, instead of all the command container as well. The following are built for our web server, with their exposed ports detailed:

- **nginx** - `:80`
- **mysql** - `:3306`
- **php** - `:9000`
- **redis** - `:6379`
- **mailhog** - `:8025`

## PREPARING APP

The Laravel source is empty at this moment. To create, copy your existing laravel directory inside the `src` folder or create new one using the line below.
```
docker-compose run --rm composer create-project laravel/laravel .
```
just make sure the root of your laravel project is the `src` folder.
NOTE: `src` folder in ignored so create separate version control inside `src` folder

## LOCAL MYSQL

For Database:
``` (php)
DB_HOST:127.0.0.1 to DB_HOST:mysql

# ex:
DB_CONNECTION=mysql
DB_HOST=mysql
DB_PORT=3306
DB_DATABASE=homestead
DB_USERNAME=homestead
DB_PASSWORD=secret
```
For the mysql credentials follow from the `docker-compose.yml` under mysql container.

Additional commands for containers:

```
docker-compose run --rm composer install
docker-compose run --rm npm run dev
docker-compose run --rm artisan migrate
```

If you want persistent MySQL storage
1. Create a `mysql` folder in the project root, alongside the `nginx` and `src` folders.

Your app is running by visiting [localhost](http://localhost).

## Mailhog

The current version of Laravel (8 as of today) uses MailHog as the default application for testing email sending and general SMTP work during local development. Using the provided Docker Hub image, getting an instance set up and ready is simple and straight-forward. The service is included in the `docker-compose.yml` file, and spins up alongside the webserver and database services.
To see the dashboard and view any emails coming thriugh the system, visit [localhost:8025](http://localhost:8025)

## AWS RDS

To connect using rds by running 

```
docker-compose up -d --build prod_site

# laravel .env
DB_HOST=rds_mysql
DB_PORT=33061

# docker-compose.yml -> rds_mysql
DB_NAME: homestead
DB_USER: homestead
DB_PASSWORD: secret
DB_HOST: <endpoint-of-rds-here>.rds.amazonaws.com
```

Then change the `DB_HOST` value in laravel `.env` to `127.0.0.1`

## RUNNING MULTI=DOCKER

Rename the `container_name` with suffix and if nessesary change also the exposed port respectively.

This is important
```
networks: 
  laravel:
    driver: bridge
```

## Troubleshooting

### For some reason, the `src/vendor/bin` folder is empty.

```
# Window
xcopy /e /v .\bin-backup\* .\src\vendor\bin\

# Linux
cp bin-backup/* src/vendor/bin/
```

### Storage permission denied

```
docker-compose exec php chmod -R 777 /var/www/html/storage
```